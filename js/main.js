
(function($) {
	"use strict";

	/*  [ jQuery Upload File ]
        - - - - - - - - - - - - - - - - - - - - */
	
	$(document).ready(function() {

		
	  

	    

	    
		/*  [ Menu Moblie ]
        - - - - - - - - - - - - - - - - - - - - */
		var toggles = document.querySelectorAll(".c-hamburger");
		  	for (var i = toggles.length - 1; i >= 0; i--) {
		    	var toggle = toggles[i];
		    toggleHandler(toggle);
		  	};
		function toggleHandler(toggle) {
		    toggle.addEventListener( "click", function(e) {
		    	e.preventDefault();
		    	(this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
		    });
		}

		
		 /*  [ Search Form ]
        - - - - - - - - - - - - - - - - - - - - */
	    $('.search-icon a').on('click', function (e){
	    	e.preventDefault();
			$( this ).parent().find('.form-search').fadeToggle();
			$( this ).parent().find('#searchForm').fadeToggle();
		});

		$('.form-search').on('click', function (e){
	    	e.preventDefault();
			$( this ).fadeToggle();
			$( this ).parent().find('#searchForm').fadeToggle();
		});

		$(".raised > span").each(function() {
			$(this)
				.data("origWidth", $(this).width())
				.width(0)
				.animate({
					width: $(this).data("origWidth")
				}, 1200);
		});

		/*  [ Main Menu ]
        - - - - - - - - - - - - - - - - - - - - */
		$( '.c-hamburger' ).on( 'click', function() {
            $( this ).parents( '.main-menu' ).toggleClass('open');
            $( 'body' ).toggleClass( 'menu-open' );
        });
        $( 'html' ).on( 'click', function(e) {
            if( $( e.target ).closest( '.main-menu.open' ).length == 0 ) {
                $( '.main-menu' ).removeClass( 'open' );
                $( 'body' ).removeClass( 'menu-open' );
                $( '.c-hamburger' ).removeClass('is-active');
            }
        });

       

        /*  [ Header Fixed ]
        - - - - - - - - - - - - - - - - - - - - */
        if ($(window).width()<992) {
	        $(window).scroll(function(){
	        	if($(this).scrollTop()>0){
					$('#header').addClass('fixed');
			    }else{
					$('#header').removeClass('fixed');
			    }
	        });
        }

        /*  [ Sub Menu ]
    	- - - - - - - - - - - - - - - - - - - - */
        $( '.main-menu ul > li' ).on('click', function () {
			$( this ).find('.sub-menu').slideToggle();
		});

	   
        

		
	
		var toolbarOptions = [
            ['bold'],        // toggled buttons                       

            [{ 'size': [false, 'large', 'huge'] }],  // custom dropdown
            [ 'link' ],  
            [ 'image' ],   
            [ 'video' ],
            ['clean'],
        ];
		
		$('.create-perk').on('click', function (e){
			e.preventDefault();
			$(this).parent().fadeOut(0);
			$(this).parent().parent().find('.start-form').fadeIn();
		});
		$('.view-fees').on('click', function (e){
			e.preventDefault();
			$(this).parent().parent().find('.spopup-bg').fadeIn();
			$(this).parent().parent().find('.fees-popup').fadeIn();
		});
		$('.spopup-bg').on('click', function (e){
			e.preventDefault();
			$(this).fadeOut();
			$(this).parent().find('.fees-popup').fadeOut();
			$(this).parent().find('.item-popup').fadeOut();
		});
		$('.spopup-close').on('click', function (e){
			e.preventDefault();
			$(this).parent().parent().fadeOut();
			$(this).parent().parent().parent().find('.spopup-bg').fadeOut();
		});
		$('.item-cancel').on('click', function (e){
			e.preventDefault();
			$(this).parent().parent().fadeOut();
			$(this).parent().parent().parent().find('.spopup-bg').fadeOut();
		});
		$('.add-item').on('click', function (e){
			e.preventDefault();
			$(this).parent().find('.spopup-bg').fadeIn();
			$(this).parent().find('.item-popup').fadeIn();
		});
		$('.connect-fb').on('click', function (e){
			e.preventDefault();
			$(this).fadeOut(0);
			$(this).parent().find('.fb-content').fadeIn();
		});
		$('.fb-content a').on('click', function (e){
			e.preventDefault();
			$(this).parent().fadeOut(0);
			$(this).parent().parent().find('.connect-fb').fadeIn();
		});
		$('.add-reward').on('click', function (e){
			e.preventDefault();
			$("#import").append($("#itemform").html());
		});
		$("#import").bind("DOMSubtreeModified", function() {
			$('.reward-delete').on('click', function (e){
				e.preventDefault();
				$(this).parent().parent().fadeOut(0);	
			});
		});
	});

	
	$(window).on('load', function() {
		$( '.grid' ).each( function() {
	    	var $grid = $('.grid').isotope({
			  itemSelector: '.filterinteresting',
			  layoutMode: 'fitRows',
			  getSortData: {
			    name: '.name',
			    symbol: '.symbol',
			    number: '.number parseInt',
			    category: '[data-category]',
			    weight: function( itemElem ) {
			      var weight = $( itemElem ).find('.weight').text();
			      return parseFloat( weight.replace( /[\(\)]/g, '') );
			    }
			  }
			});

			// filter functions
			var filterFns = {
			  // show if number is greater than 50
			  numberGreaterThan50: function() {
			    var number = $(this).find('.number').text();
			    return parseInt( number, 10 ) > 50;
			  },
			  // show if name ends with -ium
			  ium: function() {
			    var name = $(this).find('.name').text();
			    return name.match( /ium$/ );
			  }
			};

			// bind filter button click
			$('.filter-theme').on( 'click', 'button', function() {
			  var filterValue = $( this ).attr('data-filter');
			  // use filterFn if matches value
			  filterValue = filterFns[ filterValue ] || filterValue;
			  $grid.isotope({ filter: filterValue });
			});



			// change is-checked class on buttons
			$('.campaign-tabs').each( function( i, buttonGroup ) {
			  var $buttonGroup = $( buttonGroup );
			  $buttonGroup.on( 'click', 'button', function() {
			    $buttonGroup.find('.is-checked').removeClass('is-checked');
			    $( this ).addClass('is-checked');
			  });
			});
	    });
	});
})(jQuery);

      var $container = $('.sidebar-nav');
$container.find('.sub-menu-sidebar').hide();

$container.find('.toggle').click(function (e) {
    var $menu = $(this).next('.sub-menu-sidebar').slideToggle();
    $('ul.sub-menu-sidebar', $container).not($menu).slideUp();
    
    e.preventDefault();
});
      

